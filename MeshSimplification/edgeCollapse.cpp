
#include "edgeCollapse.hpp"

#include <CGAL/Combinatorial_map.h>
#include <CGAL/Linear_cell_complex.h>

EdgeCollapse::EdgeCollapse( LCC_3* lcc )
{
  m_lcc = lcc;
  m_edgeSelector = new EdgeSelection( lcc ); 
}

EdgeCollapse::~EdgeCollapse()
{
  delete m_edgeSelector;
}

bool checkExistingNeighbor( LCC_3* lcc, LCC_3::Dart_handle point_dh, LCC_3::Dart_handle insert_dh )
{
  // for all neighbors of the insert point
  for  (LCC_3::Dart_of_cell_range<0>::iterator
        itA(lcc->darts_of_cell<0>(insert_dh).begin());
        itA!=lcc->darts_of_cell<0>(insert_dh).end(); ++itA)
  {
    Kernel::Vector_3 PtDiff = LCC_3::point(itA->beta(2)) - LCC_3::point(point_dh); // LCC_3::point(itA->beta(2)) == LCC_3::point(point_dh) ) 
    double diff = PtDiff.x()*PtDiff.x() + PtDiff.y()*PtDiff.y() + PtDiff.z()*PtDiff.z();

    if( diff < 0.0001 ) // point already exists in neighbors
    {
      return true;
    }
  }

  return false;
}

void processEndOfEdge( LCC_3* lcc, LCC_3::Dart_handle insert_dh, LCC_3::Dart_handle dhA )
{
  std::vector<LCC_3::Dart_handle> dartsToRemove;

  for  (LCC_3::Dart_of_cell_range<0>::iterator
        itA(lcc->darts_of_cell<0>(dhA).begin());      // darts_of_cell() : all darts linked to the 0-cell of the given cell
        itA!=lcc->darts_of_cell<0>(dhA).end(); ++itA)
  {
    // link neighbors to barycenter
    if( itA!=insert_dh->beta(2) && !checkExistingNeighbor(lcc, itA->beta(1), insert_dh ) )   // to avoid processing barycenter itself
    {
      lcc->insert_dangling_cell_1_in_cell_2( itA->beta(2), LCC_3::point(insert_dh) );
    }

    // edge to be removed
    if( CGAL::is_removable<LCC_3,1>(*lcc,itA) && itA!=dhA )
    {
      dartsToRemove.push_back(itA);
    }
  }

  for(int i=0;i<dartsToRemove.size();i++)
  {
    CGAL::remove_cell<LCC_3,1>(*lcc,dartsToRemove[i]); // remove a 1-cell = edge
  }
}

void removeEdge( LCC_3* lcc, LCC_3::Dart_range::iterator it )
{
  // Add Barycenter of the edge
  LCC_3::Dart_handle barycenter_dh = lcc->insert_barycenter_in_cell<1>(it); // removes "it"

  LCC_3::Dart_handle dhA = barycenter_dh->beta(2);
  LCC_3::Dart_handle dhB = barycenter_dh->beta(0);

  // Add darts linking barycenter:

  // end A of the edge : for all neighbors of dhA
  processEndOfEdge(lcc, barycenter_dh, dhA); // barycenter_dh = dhA->beta(2)

  // end B of the edge : for all neighbors of it
  processEndOfEdge(lcc, dhB->beta(2), dhB);

  // remove former edge linked to barycenter
  if( CGAL::is_removable<LCC_3,1>(*lcc,barycenter_dh) ) CGAL::remove_cell<LCC_3,1>(*lcc,barycenter_dh);

}

void EdgeCollapse::collapseEdges(int collapseRate)
{
  unsigned int treated = m_lcc->get_new_mark ();

  m_edgeSelector->selectEdgesRandom(collapseRate, treated); // will mark edges to be removed

  std::cout<<"> Removing selected edges..";

  // for all edges
  for (LCC_3::Dart_range::iterator it (m_lcc->darts().begin ());
         m_lcc->number_of_marked_darts (treated) > 0; ++it)
  {
    if (m_lcc->is_marked (it, treated))
    {
      removeEdge(m_lcc, it);
      m_lcc->unmark(it, treated); // Unmark dart
    }
  }

  CGAL_assertion (m_lcc->is_whole_map_unmarked (treated));
  CGAL_assertion (m_lcc->is_valid ());
  m_lcc->free_mark (treated);

  std::cout<<"DONE"<<std::endl;
} 
